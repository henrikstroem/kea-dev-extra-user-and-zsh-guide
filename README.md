# Extra User and ZSH Guide for KEA-DEV

This guide is a quick instruction in creating extra users and changing their shell to the more fully-featured `zsh`.


## Install ZSH

Make sure `zsh` is installed:

```bash
sudo apk add zsh
```


## Create A New User

Log on to kea-dev using your regular user.
Add a new user - replace `alterego` with an appropriate user name.

```bash
sudo adduser alterego
```

Type the new user's password twice, pressing `ENTER` after the password.


## Copy SSH Keys and Set Permissions

Copy the SSH keys to the new user:

```bash
sudo cp -r ~/.ssh /home/alterego/
```

Next, set appropriate permissions so the new user can access the keys:

```bash
sudo chown -R alterego:alterego /home/alterego/.ssh
```


## Set Shell For The New User

To configure the `zsh` shell for the new user, we must edit a system file:

```bash
sudo vim /etc/passwd
```

This will open the `vim` editor.
To find the correct line, use the search function by pressing `ESC` and typing `/alterego`.

This will bring you to the correct line. Press `ENTER`.

Now press `A` to append to the line, step back using `BACKSPACE`, and make sure the line ends with `/bin/zsh`.


## Give SUDO Permission

The new user needs `sudo` permissions, so edit use `visudo` as before:

```bash
sudo visudo
```

Press `/root` and `n` for next, that should bring you to the place you added the first user to the file.

Press `ENTER`, and then `o` to insert a new line.

Insert the line:

```text
alterego ALL=(ALL) NOPASSWD:ALL
```

Exit and save by pressing `ESC` followed by `:wq`.


## Test the New User

Open a new terminal window, and access `kea-dev` as the new user:

```bash
ssh alterego@kea-dev
```

> Perform the final steps as the new user.

## Install ZSH Plugins

Create a folder for the plug-ins:

```bash
mkdir .plugins
```

Clone this repository:

```bash
git clone https://gitlab.com/henrikstroem/kea-dev-extra-user-and-zsh-guide.git
```

Unzip the plugins:

```bash
unzip kea-dev-extra-user-and-zsh-guide/zsh-plugins.zip -d .plugins/
```

Exit and log back in.
You can find examples for your `zprofile`, `.zshrc`, `.vimrc`, and `.tmux.conf` file on this link:

https://gitlab.com/users/henrikstroem/snippets
